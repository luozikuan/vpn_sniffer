#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "guesspwd.h"
#include "crypt.h"
#include "global.h"

/* Structure for the binary output from genkeys - used by asleap to read the
   file. */
struct hashpass_rec {
     unsigned char rec_size;
     char *password;
     unsigned char hash[16];
} __attribute__ ((packed));

/* Structure for the index file from genkeys */
struct hashpassidx_rec {
     unsigned char hashkey[2];
     off_t offset;
     unsigned long long int numrec;
} __attribute__ ((packed));

char dict_path[255];

/* Accepts the populated asleap_data structure with the challenge and 
   response text, and our guess at the full 16-byte hash (zpwhash). Returns 1
   if the hash does not match, 0 if it does match. */
int testchal(struct asleap_data *asleap_ptr, unsigned char *zpwhash)
{

     unsigned char cipher[8];

     DesEncrypt(asleap_ptr->challenge, zpwhash, cipher);
     if (memcmp(cipher, asleap_ptr->response, 8) != 0)
          return (1);

     DesEncrypt(asleap_ptr->challenge, zpwhash + 7, cipher);
     if (memcmp(cipher, asleap_ptr->response + 8, 8) != 0)
          return (1);

     /* else - we have a match */
     return (0);
}

int IsBlank(char *s)
{
     int len, i;
     if (s == NULL) {
          return (1);
     }

     len = strlen(s);

     if (len == 0) {
          return (1);
     }

     for (i = 0; i < len; i++) {
          if (s[i] != ' ') {
               return (0);
          }
     }
     return (1);
}

/* Brute-force all the matching NT hashes to discover the clear-text password */
int getmschappw(struct asleap_data *asleap_ptr)
{

     unsigned char zpwhash[16] = {
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
     };
     struct hashpass_rec rec;
     struct hashpassidx_rec idxrec;
     char password_buf[MAX_NT_PASSWORD];
     int passwordlen, i;
     FILE *buffp, *idxfp;

     /* If the user passed an index file for our reference, fseek to
        map the file and perform lookups based on indexed offsets.
        If there is no index file, perform a linear search. 
     */

     /* Use referenced index file for hash searches */

     memset(&idxrec, 0, sizeof(idxrec));

     if ((idxfp = fopen(asleap_ptr->dictidx, "rb")) == NULL) {
          perror("[getmschappw] Cannot open index file");
          return (-1);
     }

     /* Open the file with a buffered file handle */
     if ((buffp = fopen(asleap_ptr->dictfile, "rb")) == NULL) {
          perror("[getmschappw] fopen");
          fclose(idxfp);
          return (-1);
     }

     /* Read through the index file until we find the entry that matches
        our hash information */
     while (idxrec.hashkey[0] != asleap_ptr->endofhash[0] ||
            idxrec.hashkey[1] != asleap_ptr->endofhash[1]) {

          if (fread(&idxrec, sizeof(idxrec), 1, idxfp) != 1) {
               /* Unsuccessful fread, or EOF */
               /* printf("\tReached end of index file.\n"); */
               fclose(idxfp);
               fclose(buffp);
               return (0);
          }
     }

     /* The offset entry in the idxrec struct points to the first
        hash+pass record in the hash+pass file that matches our offset.  The
        idxrec struct also tells us how many entries we can read from the
        hash+pass file that match our hashkey information.  Collect records
        from the hash+pass file until we read through the number of records
        in idxrec.numrec */

     /* fseek to the correct offset in the file */
     if (fseeko(buffp, idxrec.offset, SEEK_SET) < 0) {
          perror("[getmschappw] fread");
          fclose(buffp);
          fclose(idxfp);
          return (-1);
     }

     for (i = 0; i < idxrec.numrec; i++) {

          memset(&rec, 0, sizeof(rec));
          memset(&password_buf, 0, sizeof(password_buf));
          fread(&rec.rec_size, sizeof(rec.rec_size), 1, buffp);

          /* The length of the password is the record size, 16 for the hash,
             1 for the record length byte. */
          passwordlen = rec.rec_size - 17;

          /* Check for corrupt data conditions, prevent segfault */
          if (passwordlen > MAX_NT_PASSWORD) {
               fprintf(stderr,
                       "Reported password length (%d) is longer than "
                       "the max password length (%d).\n",
                       passwordlen, MAX_NT_PASSWORD);
               return (-1);
          }

          /* Gather the clear-text password from the dict+hash file,
             then grab the 16 byte hash */
          fread(&password_buf, passwordlen, 1, buffp);
          fread(&zpwhash, sizeof(zpwhash), 1, buffp);

          /* Test the challenge and compare to our hash */
          if (testchal(asleap_ptr, zpwhash) == 0) {
               /* Found a matching password!  Store in the asleap_ptr struct */
               memcpy(asleap_ptr->nthash, zpwhash, 16);
               strncpy(asleap_ptr->password, password_buf,
                       strlen(password_buf));
               fclose(buffp);
               fclose(idxfp);
               /* success */
               return (1);
          }
     }

     /* Could not find a match - bummer */
     fclose(buffp);
     fclose(idxfp);

     return (0);
}

int gethashlast2(struct asleap_data *asleap_ptr)
{

     int i;
     unsigned char zpwhash[7] = { 0, 0, 0, 0, 0, 0, 0 };
     unsigned char cipher[8];

     for (i = 0; i <= 0xffff; i++) {
          zpwhash[0] = i >> 8;
          zpwhash[1] = i & 0xff;

          DesEncrypt(asleap_ptr->challenge, zpwhash, cipher);
          if (memcmp(cipher, asleap_ptr->response + 16, 8) == 0) {
               /* Success in calculating the last 2 of the hash */
               /* debug - printf("%2x%2x\n", zpwhash[0], zpwhash[1]); */
               asleap_ptr->endofhash[0] = zpwhash[0];
               asleap_ptr->endofhash[1] = zpwhash[1];
               return 0;
          }
     }

     return (1);
}

void asleap_reset(struct asleap_data *asleap)
{

     /* memset(asleap->username, 0, sizeof(asleap->username)); */
     memset(asleap->challenge, 0, sizeof(asleap->challenge));
     memset(asleap->response, 0, sizeof(asleap->response));
     memset(asleap->endofhash, 0, sizeof(asleap->endofhash));
     memset(asleap->password, 0, sizeof(asleap->password));
     /* memset(asleap->pptpauthchal, 0, sizeof(asleap->pptpauthchal)); */
     /* memset(asleap->pptppeerchal, 0, sizeof(asleap->pptppeerchal)); */
//    memset(asleap->pptpchal, 0, sizeof(asleap->pptpchal));
//    memset(asleap->pptppeerresp, 0, sizeof(asleap->pptppeerresp));
     /* asleap->leapchalfound = asleap->leaprespfound = 0; */
     /* asleap->leapsuccessfound = 0; */
     /* asleap->pptpchalfound = asleap->pptprespfound = 0; */
     /* asleap->pptpsuccessfound = 0; */
}

int attack_leap(struct asleap_data *asleap)
{

     int getmschappwret = 0;

     /* if (gethashlast2(asleap)) { */
     /*      asleap_reset(asleap); */
     /*      return -1; */
     /* } */

     getmschappwret = getmschappw(asleap);

     if (getmschappwret == 1) {
          /* Success! */
          return 0;
     } else if (getmschappwret == 0) {
          /* No matching hashes found */
          return 1;
     } else {
          /* Received an error */
          return -1;
     }
     /* gcc need a return value, make it happy */
     return -1;
}

int filter(const struct dirent *ent)
{
     int len = strlen(ent->d_name);
     if (len < 4)
          return 0;
     return (0 == strncmp(ent->d_name + len - 4, ".txt", 4));
}

int asleapBrute(const byte *challenge,
                const byte *response,
                char *password)
{
     struct asleap_data asleap;
     memset(&asleap, 0, sizeof(asleap));
     memcpy(asleap.challenge, challenge, 8);
     memcpy(asleap.response, response, 24);
     
     if (gethashlast2(&asleap)) {
          asleap_reset(&asleap);
          return -1;
     }
     /* use milti-file guess */
     int n;
     struct dirent **namelist;
     n = scandir(dict_path, &namelist, filter, alphasort);
     if (n < 0) {
          LOG("scan %s error", dict_path);
          exit(EXIT_FAILURE);
     }
     int i;
     char path[255] = {'\0'};
     struct stat buf;
     int txtModTime, datModTime;
     for (i = 0; i < n; i++) {
          strncpy(path, dict_path, 255);
          strcat(path, namelist[i]->d_name);
          stat(path, &buf);
          txtModTime = buf.st_mtime;

          strncpy(path + strlen(path) - 4, ".dat", 4);
          stat(path, &buf);
          datModTime = buf.st_mtime;

          if (0 != access(path, F_OK)) {
               fprintf (stderr, "%s need regenerate\n", path);
               free(namelist[i]);
               continue;
          }
          if (datModTime < txtModTime) {
               printf ("still use %s\n", path);
          }

          strncpy(asleap.dictfile, path, 255);
          strncpy(path + strlen(path) - 4, ".idx", 4);
          strncpy(asleap.dictidx, path, 255);

          /* printf ("asleap challenge: "); */
          /* showByte(asleap.challenge, 8); */
          /* printf ("asleap response: "); */
          /* showByte(asleap.response, 24); */
          /* printf ("asleap endofhash: "); */
          /* showByte(asleap.endofhash, 2); */
          /* printf ("asleap nthash: "); */
          /* showByte(asleap.nthash, 16); */

          /* printf ("%s\n", asleap.dictfile); */
          /* printf ("%s\n\n", asleap.dictidx); */

          int ret = attack_leap(&asleap);
          if (0 == ret) {
               strncpy(password, asleap.password, 100);
               for (; i < n; i++)
                    free(namelist[i]);
               free(namelist);
               return 0;
          }

          free(namelist[i]);
     }
     free(namelist);

     return -1;
}
