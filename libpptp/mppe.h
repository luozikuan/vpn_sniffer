#ifndef _MPPE_H_
#define _MPPE_H_

#include <stdbool.h>
#include <openssl/rc4.h>
#include "global.h"
#include "consult.h"
#include "tunnel.h"

/* struct ppp_mppe_state.bits definitions */
#define MPPE_BIT_A	0x80	/* Encryption table were (re)inititalized */
#define MPPE_BIT_B	0x40	/* MPPC only (not implemented) */
#define MPPE_BIT_C	0x20	/* MPPC only (not implemented) */
#define MPPE_BIT_D	0x10	/* This is an encrypted frame */

#define MPPE_BIT_FLUSHED	MPPE_BIT_A
#define MPPE_BIT_ENCRYPTED	MPPE_BIT_D

#define MPPE_BITS(p) ((p)[0] & 0xf0)
#define MPPE_CCOUNT(p) ((((p)[0] & 0x0f) << 8) + (p)[1])
#define MPPE_CCOUNT_SPACE 0x1000	/* The size of the ccount space */

#define MPPE_KEY_LEN 16
struct MPPE
{
     bool stateless;
     int ccount;
     unsigned keylen;           /* 8 or 16 bytes */
     int keybits;               /* 40, 56 or 128 bits */
     byte MasterKey[MPPE_KEY_LEN];
     byte SessionKey[MPPE_KEY_LEN];
     RC4_KEY RC4Key;
};

struct Tunnel;

/* decrypt data */
void getCOMPpacket(struct Tunnel *tunnel,
		   u8 *header,
                   int len,
                   bool fromServer);
void decompInit(struct Tunnel *tunnel);

/* /\* mppe declaration *\/ */
/* int  mppe_decomp_init(); */
/* void mppe_decomp_reset(); */
/* int  mppe_decompress(void *arg, unsigned char *ibuf, int isize, unsigned char *obuf, int osize); */

#endif /* _MPPE_H_ */
