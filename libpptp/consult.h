#ifndef _CONSULT_H_
#define _CONSULT_H_

#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "global.h"
#include "mppe.h"
#include "tunnel.h"

struct Tunnel;

/* cope LCP, Authentication, CCP, IPCP */
struct LCP
{
     int mru;
     int auth_protocol;         /* see global.h for detail */
#define MS_CHAP_2       129     /* Micro$oft CHAP v2 */
     int auth_algorithm;

     bool pfc;                  /* Protocol Field Compression */
     bool acfc;                 /* Address and Control Field Compression */
};

struct CHAP
{
#define OK                                0
#define ERROR_RESTRICTED_LOGON_HOURS      646
#define ERROR_ACCT_DISABLED               647
#define ERROR_PASSWD_EXPIRED              648
#define ERROR_NO_DIALIN_PERMISSION        649
#define ERROR_AUTHENTICATION_FAILURE      691
#define ERROR_CHANGING_PASSWORD           709
     int status;
     char challenge[33];          /* hex string form, 32 octet, end with '\0' */
     byte challengeByte[16];      /* byte form, 16 octet */
     char servername[100];
     char response[99];         /* hex string form, 98 octet, end with '\0' */
     byte responseByte[49];
     char username[100];
     char password[100];
     bool gotPassword;
};

/*
 * This is not nice ... the alternative is a bitfield struct though.
 * And unfortunately, we cannot share the same bits for the option
 * names above since C and H are the same bit.  We could do a u_int32
 * but then we have to do a htonl() all the time and/or we still need
 * to know which octet is which.
 */
#define MPPE_H_BIT             0x01    /* Stateless (in a different byte) */
#define MPPE_M_BIT             0x80    /* 56-bit, not supported */
#define MPPE_S_BIT             0x40    /* 128-bit */
#define MPPE_L_BIT             0x20    /* 40-bit */
#define MPPE_D_BIT             0x10    /* Obsolete, usage unknown */
#define MPPE_C_BIT             0x01    /* MPPC */
#define CCP_MPPE_IS_H(f) ((f[0])&MPPE_H_BIT)
#define CCP_MPPE_IS_M(f) ((f[3])&MPPE_M_BIT)
#define CCP_MPPE_IS_S(f) ((f[3])&MPPE_S_BIT)
#define CCP_MPPE_IS_L(f) ((f[3])&MPPE_L_BIT)
#define CCP_MPPE_IS_D(f) ((f[3])&MPPE_D_BIT)
#define CCP_MPPE_IS_C(f) ((f[3])&MPPE_C_BIT)

struct CCP
{
     struct {
          bool stateless;
          int bits;
          bool mppc;
     } mppe;
};

struct IPCP
{
     struct in_addr localIP;
     struct in_addr dns[2];
     struct in_addr gate;
};

void getLCPpacket(struct Tunnel *tunnel,
		  u8 *header,
                  int len,
                  bool fromServer);
void getCHAPpacket(struct Tunnel *tunnel,
		   u8 *header,
                   int len,
                   bool fromServer);
void getCCPpacket(struct Tunnel *tunnel,
		  u8 *header,
                  int len,
                  bool fromServer);
void getIPCPpacket(struct Tunnel *tunnel,
		   u8 *header,
                   int len,
                   bool fromServer);

#endif /* _CONSULT_H_ */
