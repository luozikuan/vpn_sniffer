#include <stdio.h>
#include "gre.h"
#include "pqueue.h"

#define PPP_ADDRESS(p)	(((__u8 *)(p))[0])
#define PPP_CONTROL(p)	(((__u8 *)(p))[1])
#define PPP_PROTOCOL(p)	((((__u8 *)(p))[2] << 8) + ((__u8 *)(p))[3])

/*
 * Significant octet values.
 */
#define	PPP_ALLSTATIONS	0xff	/* All-Stations broadcast address */
#define	PPP_UI		0x03	/* Unnumbered Information */
#define	PPP_FLAG	0x7e	/* Flag Sequence */
#define	PPP_ESCAPE	0x7d	/* Asynchronous Control Escape */
#define	PPP_TRANS	0x20	/* Asynchronous transparency modifier */

#define WRAPPED(curseq, lastseq)\
	((((curseq) & 0xffffff00) == 0) &&\
	(((lastseq) & 0xffffff00) == 0xffffff00))

pqueue_t *pq_server_head = NULL, *pq_server_tail = NULL; /* for re-order packet from server */
pqueue_t *pq_client_head = NULL, *pq_client_tail = NULL; /* for re-order packet from client */

typedef void (*callback_t)(struct Tunnel *tunnel, byte *payload, int payloadlen, bool fromServer);
void dequeue_payload(struct Tunnel *tunnel, callback_t callback, bool fromServer);

void showgre(struct pptp_gre_header *h)
{
     if (PPTP_GRE_IS_C(h->flags))
          printf ("checksum      : 1\n");
     if (PPTP_GRE_IS_R(h->flags))
          printf ("routing       : 1\n");
     if (PPTP_GRE_IS_K(h->flags))
          printf ("key bit       : 1\n");
     printf ("protocal      : 0x%x\n", h->protocol);
}

void copeGREpacket(struct Tunnel *tunnel, byte *payload, int payloadlen, bool fromServer)
{
     /* check whether payload begin with "ff03", if true, omit it */
     if (PPP_ALLSTATIONS == PPP_ADDRESS(payload) &&
         PPP_UI == PPP_CONTROL(payload)) {
          if (payloadlen < 3)
               return ;
          /* chop off address/control */
          payload += 2;
          payloadlen -= 2;
     }
     
     int proto = 0;
     if (payload[0] & 1) {
          /* protocol is compressed */
          proto = payload[0];
          payload++;
          payloadlen--;
     } else {
          if (payloadlen < 2)
               proto = 0;
          proto = (payload[0] << 8) + (payload[1]);
          payload += 2;
          payloadlen -= 2;
     }
     
     switch (proto) {
     case PPP_LCP:
          /* tunnel->lcp_info.get_option(); */
          getLCPpacket(tunnel, payload, payloadlen, fromServer);
          break;
     case PPP_CHAP:
          getCHAPpacket(tunnel, payload, payloadlen, fromServer);
          break;
     case PPP_CCP:
          getCCPpacket(tunnel, payload, payloadlen, fromServer);
          break;
     case PPP_IPCP:
          getIPCPpacket(tunnel, payload, payloadlen, fromServer);
          break;
     case PPP_COMP:
          getCOMPpacket(tunnel,payload, payloadlen, fromServer);
          break;
     default:
          /* printf ("unknown\n"); */
          break;
     }
}

void getGREpacket(struct Tunnel *tunnel, struct pptp_gre_header *header, int len, bool fromServer)
{
     static bool client_first = true, server_first = true;
     
     if (ntohs(header->protocol) != PPTP_GRE_PROTO || /* PPTP-GRE protocol for PPTP */
         PPTP_GRE_IS_C(header->flags) ||              /* flag C should be clear */
         PPTP_GRE_IS_R(header->flags) ||              /* flag R should be clear */
         !PPTP_GRE_IS_K(header->flags) ||             /* flag K should be set */
         (header->flags&0xF) != 0)                    /* routing and recursion ctrl = 0 */
          /* if invalid, discard this packet */
          return ;

     byte *payload = (byte*)header + sizeof(struct pptp_gre_header);
     int payloadlen = ntohs(header->payload_len);/* len - sizeof(struct pptp_gre_header); */
     int status = len - sizeof(struct pptp_gre_header);

     /* test if acknowledgement present */
     if (!PPTP_GRE_IS_A(header->ver)) {
          payload -= sizeof(header->ack);
          status += sizeof(header->ack);
     }/*  else { */
     /*      __u32 ack; */

     /*      /\* ack in different place if S = 0 *\/ */
     /*      ack = PPTP_GRE_IS_S(header->flags) ? header->ack : header->seq; */

     /*      ack = ntohl(ack); */

     /*      if (ack > opt->ack_recv) */
     /*           opt->ack_recv = ack; */
     /*      /\* also handle sequence number wrap-around  *\/ */
     /*      if (WRAPPED(ack, opt->ack_recv)) */
     /*           opt->ack_recv = ack; */
     /* } */
     /* test if payload present */
     if (!PPTP_GRE_IS_S(header->flags)) {
          return ;
     }
     if (status < payloadlen) {
          fprintf(stderr, "discarding truncated packet (expected %d, got %d bytes)\n",
                  payloadlen, status);
          return ;
     }

     /* get packet seq, re-order it by pqueue */
     u_int32_t seq = ntohl(header->seq);
     if (fromServer) {
          /* check for expected sequence number */
          if ( server_first || (seq == tunnel->opt.seq_server_recv + 1)) { /* wrap-around safe */
               LOG("accepting packet %d", seq);
               server_first = false;
               tunnel->opt.seq_server_recv = seq;
               copeGREpacket(tunnel, payload, payloadlen, fromServer);
               /* get buffered packet from queue, and decode by copeGREpakcet */
               dequeue_payload(tunnel, copeGREpacket, fromServer);

               /* out of order, check if the number is too low and discard the packet. 
                * (handle sequence number wrap-around, and try to do it right) */
          } else if ( seq < tunnel->opt.seq_server_recv + 1 ||
                      WRAPPED(tunnel->opt.seq_server_recv, seq) ) {
               LOG("discarding duplicate or old packet %d (expecting %d)",
                   seq, tunnel->opt.seq_server_recv + 1);
               /* sequence number too high, is it reasonably close? */
          } else if ( seq < tunnel->opt.seq_server_recv + MISSING_WINDOW ||
                      WRAPPED(seq, tunnel->opt.seq_server_recv + MISSING_WINDOW) ) {
               LOG("buffering packet %d (expecting %d, lost or reordered)",
                   seq, tunnel->opt.seq_server_recv+1);
               pqueue_add(&pq_server_head, &pq_server_tail, seq, payload, payloadlen);
               /* get buffered packet from queue, and decode by copeGREpakcet */
               dequeue_payload(tunnel, copeGREpacket, fromServer);
               /* no, packet must be discarded */
          }
     } else {
          /* check for expected sequence number */
          if ( client_first || (seq == tunnel->opt.seq_client_recv + 1)) { /* wrap-around safe */
               LOG("accepting packet %d", seq);
               client_first = false;
               tunnel->opt.seq_client_recv = seq;
               copeGREpacket(tunnel, payload, payloadlen, fromServer);
               /* get buffered packet from queue, and decode by copeGREpakcet */
               dequeue_payload(tunnel, copeGREpacket, fromServer);

               /* out of order, check if the number is too low and discard the packet. 
                * (handle sequence number wrap-around, and try to do it right) */
          } else if ( seq < tunnel->opt.seq_client_recv + 1 ||
                      WRAPPED(tunnel->opt.seq_client_recv, seq) ) {
               LOG("discarding duplicate or old packet %d (expecting %d)",
                   seq, tunnel->opt.seq_client_recv + 1);
               /* sequence number too high, is it reasonably close? */
          } else if ( seq < tunnel->opt.seq_client_recv + MISSING_WINDOW ||
                      WRAPPED(seq, tunnel->opt.seq_client_recv + MISSING_WINDOW) ) {
               LOG("buffering packet %d (expecting %d, lost or reordered)",
                   seq, tunnel->opt.seq_client_recv+1);
               pqueue_add(&pq_client_head, &pq_client_tail, seq, payload, payloadlen);
               /* get buffered packet from queue, and decode by copeGREpakcet */
               dequeue_payload(tunnel, copeGREpacket, fromServer);
               /* no, packet must be discarded */
          }
     }
}

void dequeue_payload(struct Tunnel *tunnel, callback_t callback, bool fromServer)
{
     pqueue_t **head, **tail;
     
     u_int32_t *seq_recv;
     if (fromServer) {
          head = &pq_server_head;
          tail = &pq_server_tail;
          seq_recv = &(tunnel->opt.seq_server_recv);
     } else {
          head = &pq_client_head;
          tail = &pq_client_tail;
          seq_recv = &(tunnel->opt.seq_client_recv);
     }
     
     /* process packets in the queue that either are expected or have 
      * timed out. */
     
     while ( *head != NULL &&
             ( ((*head)->seq == *seq_recv + 1) || /* wrap-around safe */ 
               (pqueue_expiry_time(*head) <= 0) 
                  )
          ) {
          /* if it is timed out... */
          if ((*head)->seq != *seq_recv + 1 ) {  /* wrap-around safe */
               LOG("timeout waiting for %d packets", (*head)->seq - *seq_recv - 1);
          }
          LOG("accepting %d from queue", (*head)->seq);
          *seq_recv = (*head)->seq;
          callback(tunnel, (*head)->packet, (*head)->packlen, fromServer);
          pqueue_del(head, tail, (*head));
     }
}
