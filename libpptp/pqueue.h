#ifndef _PQUEUE_H_
#define _PQUEUE_H_

#include <time.h>
#include <sys/time.h>
#include <stdbool.h>

/* wait this many seconds for missing packets before forgetting about them */
#define DEFAULT_PACKET_TIMEOUT 0.03 /* vpn is very slow, maybe this value should be larger (default 0.3) */
extern int packet_timeout_usecs;

/* assume packet is bad/spoofed if it's more than this many seqs ahead */
#define MISSING_WINDOW 300

/* Packet queue structure: linked list of packets received out-of-order */
typedef struct pqueue {
     struct pqueue *next;
     struct pqueue *prev;
     int seq;
     struct timeval expires;
     unsigned char *packet;
     int packlen;
     int capacity;
} pqueue_t;

int       pqueue_add  (pqueue_t **head, pqueue_t **tail, int seq, unsigned char *packet, int packlen);
int       pqueue_del  (pqueue_t **head, pqueue_t **tail, pqueue_t *point);
int       pqueue_expiry_time (pqueue_t *entry);

#endif /* _PQUEUE_H_ */
