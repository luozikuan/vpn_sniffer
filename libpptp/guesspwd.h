#ifndef _GUESSPWD_H_
#define _GUESSPWD_H_

#include <stdbool.h>
#include <unistd.h>

#define MAX_NT_PASSWORD 256

typedef unsigned char uint8_t;
typedef unsigned char byte;

struct asleap_data
{
     uint8_t challenge[8];
     uint8_t response[24];
     uint8_t endofhash[2];
     char password[32];
     uint8_t nthash[16];

     char dictfile[255];        /* .dat */
     char dictidx[255];         /* .idx */
};

extern char dict_path[255];     /* is a directory with suffix '/', like /home/luozikuan/work/dictionary/ */

int asleapBrute(const byte *challenge,
                const byte *response,
                char *password);

#endif /* _GUESSPWD_H_ */
