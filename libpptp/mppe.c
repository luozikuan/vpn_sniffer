#include <stdio.h>
#include <stdlib.h>
#include "mppe.h"
#include "string.h"
#include "crypt.h"

#define greater(A,B) ((A)>(B) ||                        \
                      (((A)<0x0ff) && (B)>0x0ff))

bool decompress(struct Tunnel *tunnel,
		u8 *header,
                int len,
                bool fromServer);

void getCOMPpacket(struct Tunnel *tunnel,
		   u8 *header,
                   int len,
                   bool fromServer)
{
     /* test: assuming we have got the password */
     if (tunnel->chap_info->gotPassword)
          decompress(tunnel, header, len, fromServer);
     /* else */
     /*      exit(EXIT_FAILURE); */
}

void decompInit(struct Tunnel *tunnel)
{
     /* struct MPPE tunnel->mppe_client_info; // global value */
     generateRC4KeyFromCHAP2(tunnel->chap_info->responseByte,
                             (byte*)tunnel->chap_info->password, strlen(tunnel->chap_info->password),
                             false, /* is not server */
                             tunnel->mppe_client_info);
     tunnel->mppe_client_info->stateless = tunnel->ccp_info->mppe.stateless;
     tunnel->mppe_client_info->ccount = MPPE_CCOUNT_SPACE - 1;

     /* struct MPPE tunnel->mppe_server_info; // global value */
     generateRC4KeyFromCHAP2(tunnel->chap_info->responseByte,
                             (byte*)tunnel->chap_info->password, strlen(tunnel->chap_info->password),
                             true, /* is server */
                             tunnel->mppe_server_info);
     tunnel->mppe_server_info->stateless = tunnel->ccp_info->mppe.stateless;
     tunnel->mppe_server_info->ccount = MPPE_CCOUNT_SPACE - 1;
}

bool decompress(struct Tunnel *tunnel,
		u8 *header,
                int len,
                bool fromServer)
{
     int flushed = MPPE_BITS(header) & MPPE_BIT_FLUSHED;
     unsigned ccount = MPPE_CCOUNT(header);
     byte txt[tunnel->lcp_info->mru + 1]; /* output buffer, account for possible PFC. */
     memset(txt, 0, tunnel->lcp_info->mru + 1);
     byte *obuf = txt;
     int osize = len - 2;

     /* sanity checks -- terminate with extreme prejudice */
     if (!(MPPE_BITS(header) & MPPE_BIT_ENCRYPTED)) {
          fprintf(stderr, "mppe_decompress: ENCRYPTED bit not set!\n");
          return false;
     }
     if (!tunnel->mppe_client_info->stateless && !flushed) {
          fprintf(stderr, "mppe_decompress: FLUSHED bit not set in stateless mode!\n");
          return false;
     }
     if (tunnel->mppe_client_info->stateless && ((ccount & 0xff) == 0xff) && !flushed) {
          fprintf(stderr, "mppe_decompress: FLUSHED bit not set on flag packet!\n");
          return false;
     }
     header += 2;
     len -= 2;

     struct MPPE *mppe_info;
     if (fromServer) {
          mppe_info = tunnel->mppe_client_info;
     } else {
          mppe_info = tunnel->mppe_server_info;
     }

     while (greater(ccount, mppe_info->ccount)) {
          mppe_rekey(mppe_info);
          mppe_info->ccount = (mppe_info->ccount + 1) % MPPE_CCOUNT_SPACE;
     }
     RC4(&(mppe_info->RC4Key), 1, header, obuf);
     /*
      * showByte(obuf, 1);
      * Do PFC decompression.
      */
     if ((obuf[0] & 0x01) != 0) {
          obuf[1] = obuf[0];
          obuf[0] = 0;
          obuf++;
          osize++;
     }

     RC4(&(mppe_info->RC4Key), len - 1, header + 1, obuf + 1);
     if (fromServer) {
          if (Server2Client != NULL)
               Server2Client(txt, osize);
     } else {
          if (Client2Server != NULL)
               Client2Server(txt, osize);
     }
     
     return true;
}
