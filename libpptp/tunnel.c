#include <stdio.h>
#include <stdlib.h>
#include "tunnel.h"

struct Tunnel *tunnel_head = NULL, *tunnel_tail = NULL;

static bool tunnel_alloc(struct pptp_opt opt, struct Tunnel **new)
{
     struct Tunnel *newent = (struct Tunnel *)malloc(sizeof(struct Tunnel));
     newent->next = NULL;
     newent->prev = NULL;
     newent->opt = opt;
     newent->opt.seq_client_recv = 0;
     newent->opt.seq_server_recv = 0;
     newent->lcp_info = (struct LCP*)malloc(sizeof(struct LCP));
     newent->chap_info = (struct CHAP*)malloc(sizeof(struct CHAP));
     newent->ccp_info = (struct CCP*)malloc(sizeof(struct CCP));
     newent->ipcp_info = (struct IPCP*)malloc(sizeof(struct IPCP));
     newent->mppe_client_info = (struct MPPE*)malloc(sizeof(struct MPPE));
     newent->mppe_server_info = (struct MPPE*)malloc(sizeof(struct MPPE));

     *new = newent;
     if (newent
         && newent->lcp_info
         && newent->chap_info
         && newent->ccp_info
         && newent->ipcp_info
         && newent->mppe_client_info
         && newent->mppe_server_info)
          return true;
     return false;
}

static void tunnel_free(struct Tunnel *point)
{
     free(point->lcp_info);
     free(point->chap_info);
     free(point->ccp_info);
     free(point->ipcp_info);
     free(point->mppe_client_info);
     free(point->mppe_server_info);
     free(point);
}

static bool pptp_addr_equal(const struct pptp_addr pa, const struct pptp_addr pb)
{
     return (pa.call_id == pb.call_id) 
          && (pa.sin_addr.s_addr == pb.sin_addr.s_addr);
}

static bool pptp_opt_equal(const struct pptp_opt pa, const struct pptp_opt pb)
{
     return pptp_addr_equal(pa.client_addr, pb.client_addr) 
          && pptp_addr_equal(pa.server_addr, pb.server_addr);
}

bool tunnel_add(struct pptp_opt opt)
{
     struct Tunnel *newent, *point;
     if (!tunnel_alloc(opt, &newent)) {    
          return false;
     }

     for (point = tunnel_head; point != NULL; point = point->next) {
          if (pptp_opt_equal(point->opt, opt)) {
               /* queue already contains this tunnel */
               LOG("discarding duplicate tunnel");
               return true;
          }
     }

     if (tunnel_head == NULL) {
          LOG("adding tunnel to empty queue");
          tunnel_head = newent;
     } else {
          LOG("adding tunnel as tail");
          tunnel_tail->next = newent;
     }
     newent->prev = tunnel_tail;
     tunnel_tail = newent;
     
     return true;
}

struct Tunnel *tunnel_lookup(struct pptp_addr pptpaddr, bool *fromServer)
{
     struct Tunnel *p;
     for (p = tunnel_head; p != NULL; p = p->next) {
          if (pptp_addr_equal(p->opt.client_addr, pptpaddr)) {
               *fromServer = false;
               return p;
          }
          if (pptp_addr_equal(p->opt.server_addr, pptpaddr)) {
               *fromServer = true;
               return p;
          }
     }
     return NULL;
}

void tunnel_del(struct pptp_addr pptpaddr)
{
     bool fromServer;
     struct Tunnel *point = tunnel_lookup(pptpaddr, &fromServer);
     /* unlink from tunnellist */
     if (tunnel_head == point) tunnel_head = point->next;
     if (tunnel_tail == point) tunnel_tail = point->prev;
     if (point->prev) point->prev->next = point->next;
     if (point->next) point->next->prev = point->prev;

     tunnel_free(point);
}
