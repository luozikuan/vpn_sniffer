#ifndef _TUNNEL_H_
#define _TUNNEL_H_

#include <arpa/inet.h>
#include "global.h"
#include "consult.h"
#include "mppe.h"

struct pptp_addr {
     u16             call_id;
     struct in_addr  sin_addr;
};

struct pptp_opt {
     struct pptp_addr client_addr;
     struct pptp_addr server_addr;
     u32 seq_client_recv;
     u32 seq_server_recv;
};

struct LCP;
struct CHAP;
struct CCP;
struct IPCP;
struct MPPE;

struct Tunnel
{
     struct Tunnel *next;
     struct Tunnel *prev;
     
     struct pptp_opt opt;   /* distinguish tunnel_1 from tunnel_2 */
     
     struct LCP  *lcp_info;
     struct CHAP *chap_info;
     struct CCP  *ccp_info;
     struct IPCP *ipcp_info;

     struct MPPE *mppe_client_info;
     struct MPPE *mppe_server_info;
};

/* add a tunnel only have contain .opt filed */
bool tunnel_add(struct pptp_opt opt);

/* lookup which tunnel this packet is belong to,
 * and tell caller the direction of this packet */
struct Tunnel *tunnel_lookup(struct pptp_addr pptpaddr, bool *fromServer);

/* when this tunnel is expiried, it should be delete */
void tunnel_del(struct pptp_addr pptpaddr);

#endif /* _TUNNEL_H_ */
