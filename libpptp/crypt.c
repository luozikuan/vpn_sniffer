#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>
#include <openssl/md4.h>
#include <openssl/des.h>
#include <openssl/rc4.h>
#include <assert.h>
#include "crypt.h"

/*
 *Pads used in key derivation
 */
static byte SHAPad1[40] =
{
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
static byte SHAPad2[40] =
{
     0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2,
     0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2,
     0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2,
     0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2
};
/*
 *"Magic" constants used in key derivations
 *"this is the mppe master key"
 */
static byte Magic1[27] =
{
     0x54, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20, 0x74,
     0x68, 0x65, 0x20, 0x4d, 0x50, 0x50, 0x45, 0x20, 0x4d,
     0x61, 0x73, 0x74, 0x65, 0x72, 0x20, 0x4b, 0x65, 0x79
};
/*
 * "On the client side, this is the send key; on the server side, it is the receive key."
 */
static byte Magic2[84] =
{
     0x4f, 0x6e, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x69,
     0x65, 0x6e, 0x74, 0x20, 0x73, 0x69, 0x64, 0x65, 0x2c, 0x20,
     0x74, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20, 0x74, 0x68,
     0x65, 0x20, 0x73, 0x65, 0x6e, 0x64, 0x20, 0x6b, 0x65, 0x79,
     0x3b, 0x20, 0x6f, 0x6e, 0x20, 0x74, 0x68, 0x65, 0x20, 0x73,
     0x65, 0x72, 0x76, 0x65, 0x72, 0x20, 0x73, 0x69, 0x64, 0x65,
     0x2c, 0x20, 0x69, 0x74, 0x20, 0x69, 0x73, 0x20, 0x74, 0x68,
     0x65, 0x20, 0x72, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x20,
     0x6b, 0x65, 0x79, 0x2e
};
/*
 * "On the client side, this is the receive key; on the server side, it is the send key."
 */
static byte Magic3[84] =
{
     0x4f, 0x6e, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x69,
     0x65, 0x6e, 0x74, 0x20, 0x73, 0x69, 0x64, 0x65, 0x2c, 0x20,
     0x74, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20, 0x74, 0x68,
     0x65, 0x20, 0x72, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x20,
     0x6b, 0x65, 0x79, 0x3b, 0x20, 0x6f, 0x6e, 0x20, 0x74, 0x68,
     0x65, 0x20, 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x20, 0x73,
     0x69, 0x64, 0x65, 0x2c, 0x20, 0x69, 0x74, 0x20, 0x69, 0x73,
     0x20, 0x74, 0x68, 0x65, 0x20, 0x73, 0x65, 0x6e, 0x64, 0x20,
     0x6b, 0x65, 0x79, 0x2e
};


static void ToUnicode(const byte *src, int srcLen, byte *unicode, int *dstLen)
{
     assert(unicode && dstLen);
     *dstLen = 2 * srcLen;
     int i;
     for (i = 0; i < srcLen; i++) {
          unicode[2 * i] = src[i];
          unicode[2 * i + 1] = 0;
     }
}

static void NtPasswordHash(const byte *Password, int pwdLen, byte *PasswordHash)
{
     int uniLen = pwdLen * 2;
     byte unicode[uniLen];
     /* byte *unicode = (byte*)malloc(uniLen * sizeof(byte)); */
     ToUnicode(Password, pwdLen, unicode, &uniLen);
     MD4(unicode, uniLen, PasswordHash);
}

void ChallengeHash(const byte *PeerChallenge,          /* 16-octet */
                          const byte *AuthenticatorChallenge, /* 16-octet */
                          const byte *UserName, int nameLen,  
                          byte *Challenge)              /* 8-octet, out */
{
     /*
      * SHAInit(), SHAUpdate() and SHAFinal() functions are an
      * implementation of Secure Hash Algorithm (SHA-1) [11]. These are
      * available in public domain or can be licensed from
      * RSA Data Security, Inc.
      */
     byte Digest[20];

     SHA_CTX Context;
     SHA1_Init(&Context);
     SHA1_Update(&Context, PeerChallenge, 16);
     SHA1_Update(&Context, AuthenticatorChallenge, 16);

     /*
      * Only the user name (as presented by the peer and
      * excluding any prepended domain name)
      * is used as input to SHAUpdate().
      */

     SHA1_Update(&Context, UserName, nameLen);
     SHA1_Final(Digest, &Context);
     memcpy(Challenge, Digest, 8);
}

/*
 * Function expandWithParity (Key, keyWithParity)
 * insert parity every 7 bit
 *   +---------+---------+...
 *    1111110 0 000101 01 ... before
 *           ^        ^
 *           |        |
 *           p        p   (parity)
 *    1111110p 0000101p 01    after
 *   +--------+--------+...
 */
static void expandWithParity(const byte *Key, byte *keyWithParity)
{
     keyWithParity[0] = Key[0] & 0xfe;
     int i;
     for (i = 1; i < 7; i++) {
          keyWithParity[i] = ((Key[i - 1] & ((1 << i) - 1)) << (8 - i)) +
               ((Key[i] & (0xff - ((1 << (i + 1)) - 1))) >> i);
     }
     keyWithParity[7] = (Key[6] << 1) & 0xfe;
}

void DesEncrypt(const byte *Clear,           /* 8-octet */
                       const byte *Key,             /* 7-octet */
                       byte *Cypher )               /* 8-octet, out */
{
     /*
      * Use the DES encryption algorithm [4] in ECB mode [10]
      * to encrypt Clear into Cypher such that Cypher can
      * only be decrypted back to Clear by providing Key.
      * Note that the DES algorithm takes as input a 64-bit
      * stream where the 8th, 16th, 24th, etc.  bits are
      * parity bits ignored by the encrypting algorithm.
      * Unless you write your own DES to accept 56-bit input
      * without parity, you will need to insert the parity bits
      * yourself.
      */

     byte str_parity[8];
     expandWithParity(Key, str_parity);

     DES_cblock key;
     memcpy(key, str_parity, 8);
     DES_set_odd_parity(&key);

     DES_key_schedule schedule;
     DES_set_key_checked(&key, &schedule);

     const_DES_cblock input;
     memcpy(input, Clear, 8);
     DES_cblock output;
     DES_ecb_encrypt(&input, &output, &schedule, DES_ENCRYPT);
     memcpy(Cypher, output, 8);
}

static void ChallengeResponse(const byte *Challenge,    /* 8-octet */
                              const byte *PasswordHash, /* 16-octet */
                              byte *Response)     /* 24-octet, out */
{
     /* Set ZPasswordHash to PasswordHash zero-padded to 21 octets */
     byte ZPasswordHash[21];
     memset(ZPasswordHash, 0, 21);
     memcpy(ZPasswordHash, PasswordHash, 16);

     DesEncrypt(Challenge,
                ZPasswordHash + 0,
                Response + 0);
     DesEncrypt(Challenge,
                ZPasswordHash + 7,
                Response + 8);
     DesEncrypt(Challenge,
                ZPasswordHash + 14,
                Response + 16);
}

/* some encryption in MS-CHAP-2, see rfc2759 */
void GenerateNTResponse(const byte *AuthenticatorChallenge, /* 16-octet */
                        const byte *PeerChallenge,          /* 16-octet */
                        const byte *UserName, int nameLen,  
                        const byte *Password, int pwdLen,   
                        byte *Response)                     /* 24-octet, out */
{
     byte Challenge[8];
     byte PasswordHash[16];

     ChallengeHash(PeerChallenge, AuthenticatorChallenge, UserName, nameLen,
                   Challenge);
     NtPasswordHash(Password, pwdLen,
                    PasswordHash);
     ChallengeResponse(Challenge, PasswordHash,
                       Response);
}

/* derived from MS-CHAP-2, see rfc3079 */
static void GetMasterKey(const byte *PasswordHashHash, /* 16 octet */
                         const byte *NTResponse,       /* 24 octet */
                         byte *MasterKey)        /* 16 octet, out */
{
     byte Digest[20];
     memset(Digest, 0, sizeof(Digest));

     /*
      * SHSInit(), SHSUpdate() and SHSFinal()
      * are an implementation of the Secure Hash Standard.
      */
     SHA_CTX Context;
     SHA1_Init(&Context);
     SHA1_Update(&Context, PasswordHashHash, 16);
     SHA1_Update(&Context, NTResponse, 24);
     SHA1_Update(&Context, Magic1, 27);
     SHA1_Final(Digest, &Context);

     memcpy(MasterKey, Digest, 16);
}

static void GetAsymmetricStartKey(const byte *MasterKey,/* 16 octet */
                                  byte *SessionKey, /* 8-16 octet, out */
                                  int SessionKeyLength,
                                  bool IsSend,
                                  bool IsServer)
{
     byte Digest[20];
     memset(Digest, 0, sizeof(Digest));
     byte *s;

     if (IsSend) {
          if (IsServer) {
               s = Magic3;
          } else {
               s = Magic2;
          }
     } else {
          if (IsServer) {
               s = Magic2;
          } else {
               s = Magic3;
          }
     }

     /*
      * SHS was equal to SHA once in 1993,
      * changed to SHA1 since 1994
      * changed to SHA
      * SHSInit(), SHSUpdate() and SHSFinal()
      * are an implementation of the Secure Hash Standard.
      */
     SHA_CTX Context;
     SHA1_Init(&Context);
     SHA1_Update(&Context, MasterKey, 16);
     SHA1_Update(&Context, SHAPad1, 40);
     SHA1_Update(&Context, s, 84);
     SHA1_Update(&Context, SHAPad2, 40);
     SHA1_Final(Digest, &Context);

     memcpy(SessionKey, Digest, SessionKeyLength);
}

/*
 * SessionKeyLength is 8 for 40-bit keys, 16 for 128-bit keys.
 * SessionKey is the same as StartKey in the first call for
 * a given session.
 */
static void GetNewKeyFromSHA(const byte *StartKey,
                             const byte *SessionKey,
                             unsigned long SessionKeyLength,
                             byte *InterimKey)
{
     byte Digest[20];
     memset(Digest, 0, 20);

     /*
      * SHAInit(), SHAUpdate() and SHAFinal()
      * are an implementation of the Secure
      * Hash Algorithm [7]
      */
     SHA_CTX Context;
     SHA1_Init(&Context);
     SHA1_Update(&Context, StartKey, SessionKeyLength);
     SHA1_Update(&Context, SHAPad1, 40);
     SHA1_Update(&Context, SessionKey, SessionKeyLength);
     SHA1_Update(&Context, SHAPad2, 40);
     SHA1_Final(Digest, &Context);

     memcpy(InterimKey, Digest, SessionKeyLength);
}

void generateRC4KeyFromCHAP2(const byte *CHAPResponse,
                             const byte *Password, int pwdLen,
                             bool IsServer,
                             struct MPPE *mppeInfo) /* out */
{
     byte NTResponse[24];
     memcpy(NTResponse, CHAPResponse + 24, 24);
     
     byte PasswordHash[16];
     NtPasswordHash(Password, pwdLen, PasswordHash);
     
     byte PasswordHashHash[16];
     MD4(PasswordHash, 16, PasswordHashHash);

     /* byte *MasterKey = mppeInfo->MasterKey; */
     byte MasterKey[16];
     GetMasterKey(PasswordHashHash, NTResponse, MasterKey);

     byte *MasterReceiveKey = mppeInfo->MasterKey;
     /* byte MasterReceiveKey[16]; */
     if (IsServer) {            /* in-key, out-key,     , length, isSend, isServer */
          GetAsymmetricStartKey(MasterKey, MasterReceiveKey, 16, false, true);
     } else {
          GetAsymmetricStartKey(MasterKey, MasterReceiveKey, 16, false, false);
     }
     
     byte *ReceiveSessionKey = mppeInfo->SessionKey;
     GetNewKeyFromSHA(MasterReceiveKey, MasterReceiveKey, 16, ReceiveSessionKey);
     
     RC4_KEY *ReceiveRC4key = &(mppeInfo->RC4Key);
     RC4_set_key(ReceiveRC4key, 16, ReceiveSessionKey);
}

void mppe_rekey(struct MPPE *mppeInfo)
{
     byte InterimKey[16];       /* tmp var */
     /* The RC4 tables are initialized using the newly created interim key: */
     GetNewKeyFromSHA(mppeInfo->MasterKey, mppeInfo->SessionKey, 16, InterimKey);
     RC4_set_key(&(mppeInfo->RC4Key), 16, InterimKey);
     
     /* Then, the interim key is encrypted using the new tables to produce a new session key: */
     RC4(&(mppeInfo->RC4Key), 16, InterimKey, mppeInfo->SessionKey);
     
     /* Finally, the RC4 tables are re-initialized using the new session key: */
     RC4_set_key(&(mppeInfo->RC4Key), 16, mppeInfo->SessionKey);
}
