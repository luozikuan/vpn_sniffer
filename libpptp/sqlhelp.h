#ifndef _SQLHELP_H_
#define _SQLHELP_H_

/* get/save data from/to mysql */
#include <mysql/mysql.h>
#include "global.h"

bool mysqlInit();
bool saveChapInfoToMysql(char *challenge, char *response, char *username);
bool accountExists(char *username);
bool getPwdFromMysql(char *username, char *password);
bool clearPwdToMysql(char *username);
bool updatePwdToMysql(char *username, char *password);
bool mysqlClose();

#endif /* _SQLHELP_H_ */
