#ifndef _CRYPT_H_
#define _CRYPT_H_

#include <stdbool.h>
#include <openssl/rc4.h>
#include <openssl/sha.h>
#include "mppe.h"

typedef unsigned char byte;

void showByte(const byte *start, int len);

void DesEncrypt(const byte *Clear,           /* 8-octet */
                const byte *Key,             /* 7-octet */
                byte *Cypher );               /* 8-octet, out */

void ChallengeHash(const byte *PeerChallenge,          /* 16-octet */
                   const byte *AuthenticatorChallenge, /* 16-octet */
                   const byte *UserName, int nameLen,  
                   byte *Challenge);              /* 8-octet, out */

void GenerateNTResponse(const byte *AuthenticatorChallenge, /* 16-octet */
                        const byte *PeerChallenge,          /* 16-octet */
                        const byte *UserName, int nameLen,  
                        const byte *Password, int pwdLen,    
                        byte *Response);              /* 24-octet, out */

void generateRC4KeyFromCHAP2(const byte *CHAPResponse,
                             const byte *Password, int pwdLen,
                             bool IsServer,
                             struct MPPE *mppeInfo); /* out */

void mppe_rekey(struct MPPE *mppeInfo);

#endif /* _CRYPT_H_ */
