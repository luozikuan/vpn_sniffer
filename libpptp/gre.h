#ifndef _GRE_H_
#define _GRE_H_

#include <stdbool.h>
#include <netinet/in.h>
#include "global.h"
#include "consult.h"
#include "mppe.h"
#include "tunnel.h"

#define PPTP_GRE_PROTO  0x880B
#define PPTP_GRE_VER    0x1

#define PPTP_GRE_FLAG_C	0x80
#define PPTP_GRE_FLAG_R	0x40
#define PPTP_GRE_FLAG_K	0x20
#define PPTP_GRE_FLAG_S	0x10
#define PPTP_GRE_FLAG_A	0x80

#define PPTP_GRE_IS_C(f) ((f)&PPTP_GRE_FLAG_C)
#define PPTP_GRE_IS_R(f) ((f)&PPTP_GRE_FLAG_R)
#define PPTP_GRE_IS_K(f) ((f)&PPTP_GRE_FLAG_K)
#define PPTP_GRE_IS_S(f) ((f)&PPTP_GRE_FLAG_S)
#define PPTP_GRE_IS_A(f) ((f)&PPTP_GRE_FLAG_A)

struct pptp_gre_header
{
     u8 flags;
     u8 ver;
     u16 protocol;
     u16 payload_len;
     u16 call_id;
     u32 seq;
     u32 ack;
};

/*
 * fromServer means direction:
 * true: from server
 * false: to server
 */
void getGREpacket(struct Tunnel *tunnel, struct pptp_gre_header *header, int len,
                  bool fromServer);

#endif /* _GRE_H_ */
