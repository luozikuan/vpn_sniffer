#ifndef _GLOBAL_H_
#define _GLOBAL_H_

/*
 * some global value, use 'extern int aaa;' form, define aaa in global.cpp
 * and do NOT include this file in global.cpp
 * every *.h file should include this file use next pre-treatment
 */

#include <stdbool.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

typedef unsigned char __u8;
typedef unsigned short __u16;
typedef unsigned int __u32;

typedef unsigned char u_int8_t;
typedef unsigned short u_int16_t;
typedef unsigned int u_int32_t;

typedef unsigned char byte;
typedef unsigned char u_char;

/* The basic PPP frame. */
#define PPP_HDRLEN	4	/* octets for standard ppp header */
#define PPP_FCSLEN	2	/* octets for FCS */
#define PPP_MRU		1500	/* default MRU = max length of info field */

/* Protocol field values. */
#define PPP_IP          0x21    /* Internet Protocol */
#define PPP_AT          0x29    /* AppleTalk Protocol */
#define PPP_IPX         0x2b    /* IPX protocol */
#define PPP_VJC_COMP    0x2d    /* VJ compressed TCP */
#define PPP_VJC_UNCOMP  0x2f    /* VJ uncompressed TCP */
#define PPP_MP          0x3d    /* Multilink protocol */
#define PPP_IPV6        0x57    /* Internet Protocol Version 6 */
#define PPP_COMPFRAG    0xfb    /* fragment compressed below bundle */
#define PPP_COMP        0xfd    /* compressed packet */
#define PPP_MPLS_UC     0x0281  /* Multi Protocol Label Switching - Unicast */
#define PPP_MPLS_MC     0x0283  /* Multi Protocol Label Switching - Multicast */
#define PPP_IPCP        0x8021  /* IP Control Protocol */
#define PPP_ATCP        0x8029  /* AppleTalk Control Protocol */
#define PPP_IPXCP       0x802b  /* IPX Control Protocol */
#define PPP_IPV6CP      0x8057  /* IPv6 Control Protocol */
#define PPP_CCPFRAG     0x80fb  /* CCP at link level (below MP bundle) */
#define PPP_CCP         0x80fd  /* Compression Control Protocol */
#define PPP_MPLSCP      0x80fd  /* MPLS Control Protocol */
#define PPP_LCP         0xc021  /* Link Control Protocol */
#define PPP_PAP         0xc023  /* Password Authentication Protocol */
#define PPP_LQR         0xc025  /* Link Quality Report protocol */
#define PPP_CBCP        0xc029  /* Callback Control Protocol */
#define PPP_CHAP        0xc223  /* Cryptographic Handshake Auth. Protocol */

#if defined DEBUG
#  define LOG(format, args...)                          \
     do {                                               \
          printf("%s<%d>: ", __FILE__, __LINE__);       \
          printf(format "\n", ##args);                  \
     } while (0)
#else
#  define LOG(format, args...) 
#endif

extern void showByte(const byte *start, int len);
extern void print_payload(const u_char *payload, int len);

typedef void (*decode_t)(byte *payload, int payloadlen);
extern decode_t Server2Client, Client2Server;

#endif /* _GLOBAL_H_ */
