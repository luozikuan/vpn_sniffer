#ifndef _PPTP_H_
#define _PPTP_H_

#ifdef __cplusplus
extern "C" {
#endif

#define PPP_IP          0x21    /* Internet Protocol */
#define PPP_AT          0x29    /* AppleTalk Protocol */
#define PPP_IPX         0x2b    /* IPX protocol */
#define PPP_VJC_COMP    0x2d    /* VJ compressed TCP */
#define PPP_VJC_UNCOMP  0x2f    /* VJ uncompressed TCP */
#define PPP_MP          0x3d    /* Multilink protocol */
#define PPP_IPV6        0x57    /* Internet Protocol Version 6 */
#define PPP_COMPFRAG    0xfb    /* fragment compressed below bundle */
#define PPP_COMP        0xfd    /* compressed packet */
#define PPP_MPLS_UC     0x0281  /* Multi Protocol Label Switching - Unicast */
#define PPP_MPLS_MC     0x0283  /* Multi Protocol Label Switching - Multicast */
#define PPP_IPCP        0x8021  /* IP Control Protocol */
#define PPP_ATCP        0x8029  /* AppleTalk Control Protocol */
#define PPP_IPXCP       0x802b  /* IPX Control Protocol */
#define PPP_IPV6CP      0x8057  /* IPv6 Control Protocol */
#define PPP_CCPFRAG     0x80fb  /* CCP at link level (below MP bundle) */
#define PPP_CCP         0x80fd  /* Compression Control Protocol */
#define PPP_MPLSCP      0x80fd  /* MPLS Control Protocol */
#define PPP_LCP         0xc021  /* Link Control Protocol */
#define PPP_PAP         0xc023  /* Password Authentication Protocol */
#define PPP_LQR         0xc025  /* Link Quality Report protocol */
#define PPP_CBCP        0xc029  /* Callback Control Protocol */
#define PPP_CHAP        0xc223  /* Cryptographic Handshake Auth. Protocol */

typedef unsigned char byte;
typedef unsigned char u_char;

typedef void (*decode_t)(byte *payload, int payloadlen);
extern decode_t Server2Client, Client2Server;

extern char dict_path[255];     /* without suffix, if dict is '/home/password.txt',
                                 * this var should be '/home/password' */
void getPacket(u_char *arg,
               const struct pcap_pkthdr *pkthdr,
               const u_char *bytes);

#ifdef __cplusplus
}
#endif

#endif /* _PPTP_H_ */
