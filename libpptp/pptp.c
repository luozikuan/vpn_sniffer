#include <stdio.h>
#include <stdlib.h>
#include <pcap.h>
#include <string.h>
#include <arpa/inet.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include "global.h"
#include "gre.h"
#include "tunnel.h"
#include "pptp.h"

void getPacket(u_char *arg,
               const struct pcap_pkthdr *pkthdr,
               const u_char *bytes)
{
     /* static u_int32_t serverIP; */
     bool fromServer = false;
     
     int *cnt = (int *)arg;
     (*cnt)++;
     
     struct ether_header *ethheader = (struct ether_header *)bytes;
     int ethlen = pkthdr->caplen;
     if (ETHERTYPE_IP != ntohs(ethheader->ether_type)) /* not ip protocol */
          return ;
     
     struct iphdr *ipheader = (struct iphdr *)((char *)ethheader + sizeof(struct ether_header));
     int iplen = ethlen - sizeof(struct ether_header);

     if (6 == ipheader->protocol) { /* is tcp */
          struct tcphdr *tcpheader = (struct tcphdr *)((char *)ipheader + ipheader->ihl *4);
          if (1723 == ntohs(tcpheader->source)) { /* is pptp */
               char *pptphdr = (char*)tcpheader + tcpheader->doff * 4;
               if (0x0008 == ntohs(*(u_int16_t*)&pptphdr[8])) { /* out going reply */
                    struct pptp_opt opt;
                    opt.client_addr.call_id = ntohs(*(u_int16_t*)&pptphdr[12]);
                    opt.server_addr.call_id = ntohs(*(u_int16_t*)&pptphdr[14]);
                    opt.client_addr.sin_addr = *(struct in_addr *)&ipheader->daddr;
                    opt.server_addr.sin_addr = *(struct in_addr *)&ipheader->saddr;
                    LOG ("call id: %d, IP: %s\n",
                         opt.client_addr.call_id,
                         inet_ntoa(opt.client_addr.sin_addr));
                    LOG ("call id: %d, IP: %s\n",
                         opt.server_addr.call_id,
                         inet_ntoa(opt.server_addr.sin_addr));
                    tunnel_add(opt);
               } else if (0x0008 == ntohs(*(u_int16_t*)&pptphdr[8])) { /* call clear request */
                    /*
                     * close a tunnel
                     * see rfc2759 from detail
                     * what is set-link-info's function
                     */
               }
          }
     }

     if (47 != ipheader->protocol) /* not gre protocol */
          return ;

     struct pptp_gre_header *gre = (struct pptp_gre_header *)((char*)ipheader + ipheader->ihl * 4);
     int grelen = iplen - ipheader->ihl * 4;

     /* lookup a tunnel by call_id and src_ip */
     struct pptp_addr pptpaddr;
     pptpaddr.sin_addr = *(struct in_addr *)&ipheader->saddr;
     pptpaddr.call_id = ntohs(gre->call_id);

     struct Tunnel *tunnel = tunnel_lookup(pptpaddr, &fromServer);
     if (!tunnel) {
          fprintf(stderr, "cannot find corresponding tunnel.\n");
          return ;
     }

     getGREpacket(tunnel, gre, grelen, fromServer); /* declared in decodegre.h */
}

/* ******************** for test ******************** */

static const char line[] = "\n==============================\n";

static void saveFile(const char *path, const char *content, int len)
{
     FILE *fp = fopen(path, "a");
     if (!fp) {
          fprintf (stderr, "open file error\n");
          exit(EXIT_FAILURE);
     }
     if (0 == fwrite(content, len, 1, fp)) {
          fprintf (stderr, "write error!\n");
     }
     fclose(fp);
}

void decodeServer2Client(byte *packet, int len)
{
     char filename[] = "server2client.txt";
     saveFile(filename, (char*)packet, len);
     saveFile(filename, line, strlen(line));
}

void decodeClient2Server(byte *packet, int len)
{
     char filename[] = "client2server.txt";
     saveFile(filename, (char*)packet, len);
     saveFile(filename, line, strlen(line));
}

int main(int argc, char *argv[])
{
     char errbuf[PCAP_ERRBUF_SIZE];
#ifdef DEBUG
     /* open 'device' */
     char *filename = "data/eth0-pptp.pcapng";
     pcap_t *dev = pcap_open_offline(filename, errbuf);
     if (!dev) {
          fprintf (stderr, "open %s error\n", filename);
          exit(EXIT_FAILURE);
     }
#else
     char *dev_str;

     /* find device */
     dev_str = pcap_lookupdev(errbuf);
     if (!dev_str) {
          fprintf (stderr, "find device error!\n");
          exit(EXIT_FAILURE);
     }
     printf ("find %s\n", dev_str);
     
     pcap_t *dev = pcap_open_live(dev_str, 2048, 1, 0, errbuf);
     if (!dev) {
          fprintf (stderr, "open %s error\n", dev_str);
          exit(EXIT_FAILURE);
     }
#endif
     
     /* construct a filter */
     char filter_str[100];
     strncpy(filter_str, "", 99);
     struct bpf_program filter;
     pcap_compile(dev, &filter, filter_str, 1, 0);
     pcap_setfilter(dev, &filter);

     /* set up dictionay */
     strcpy(dict_path, "/home/luozikuan/work/");
     /* decode packet after decrypt */
     Server2Client = decodeServer2Client;
     Client2Server = decodeClient2Server;

     /* loop forever */
     int cnt = 0;
     pcap_loop(dev, -1, getPacket, (u_char*)&cnt);

     /* close device */
     pcap_close(dev);
     
     return 0;
}
