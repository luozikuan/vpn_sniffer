#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "sqlhelp.h"

static MYSQL my_connection;
static const char hostStr[]            = "localhost"; /* the host name of database */
static const char userStr[]            = "root";      /*  the user name of  database */
static const char passwdStr[]          = "wadbt";     /*  the code of database */
static const char dbStr[]              = "mydb";      /*  the database name */
/* database MUST exist before connect */
static const char tableStr[]           = "pptp";      /*  get the needed information this table */
static const char userColumnStr[]      = "account";   /*  name of user column */
static const char passwdColumnStr[]    = "passwords"; /* name of passwd column */
static const char challengeColumnStr[] = "hash1";     /*  name of challenge column */
static const char responseColumnStr[]  = "hash2";     /*  name of response column */

bool tableExists();
bool createTable();

static int execSql(char sql[200])
{
     int res;
     res = mysql_query(&my_connection, sql);
     if (!res)
          return (unsigned long)mysql_affected_rows(&my_connection);
          /* printf ("Inserted %ld rows\n", (unsigned long)mysql_affected_rows(&my_connection)); */
     else
          return -1;
          /* fprintf (stderr, "Insert error %d : %s\n", mysql_errno(&my_connection), mysql_error(&my_connection)); */
}

bool mysqlInit()
{
     bool flag = false;
     mysql_init(&my_connection);
     if(mysql_real_connect(&my_connection,
                           hostStr,
                           userStr,
                           passwdStr,
                           dbStr,
                           0,
                           NULL,
                           0)) {
          LOG ("MySQL open success.");
          flag = true;
     } else {
          flag = false;
          fprintf (stderr, "MySQL open failed.\n");
          if (mysql_errno(&my_connection))
               fprintf (stderr, "Connection error %d : %s\n", mysql_errno(&my_connection), mysql_error(&my_connection));
     }
     if (flag && !tableExists() && createTable())
          flag = true;
     else
          flag = false;
     return flag;
}

bool mysqlClose()
{
     mysql_close(&my_connection);
     LOG ("MySQL close success.");
     return true;
}

bool saveChapInfoToMysql(char *challenge, char *response, char *username)
{
     bool flag = false;
     /* mysqlInit(); */
     char sql[200];
     snprintf (sql, 200, "insert into %s values(null, '%s', null, '%s', '%s');",
               tableStr,
               username,
               challenge,
               response);
     int rows = execSql(sql);
     if (1 == rows)
          flag = true;
     /* mysqlClose(); */
     return flag;
}

bool tableExists()
{
     bool flag = false;
     /* mysqlInit(); */
     char sql[200];
     snprintf (sql, 200, "SELECT 1 FROM information_schema.TABLES WHERE table_name ='%s' limit 1;",
               tableStr);
     execSql(sql);
     MYSQL_RES *rs = mysql_store_result(&my_connection);
     if (1 == (unsigned long)mysql_num_rows(rs))
          flag = true;
     mysql_free_result(rs);
     /* mysqlClose(); */
     return flag;
}

bool createTable()
{
     bool flag = false;
     /* mysqlInit(); */
     char sql[200];
     snprintf (sql, 200, "CREATE TABLE `%s` ("
               "`id` int(11) NOT NULL AUTO_INCREMENT,"
               "`%s` varchar(40) NOT NULL,"
               "`%s` varchar(64) DEFAULT NULL,"
               "`%s` char(50) NOT NULL,"
               "`%s` char(100) NOT NULL,"
               "PRIMARY KEY (`id`)"
               ") ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8",
               tableStr,
               userColumnStr,
               passwdColumnStr,
               challengeColumnStr,
               responseColumnStr);
     int rows = execSql(sql);
     if (1 == rows)
          flag = true;
     /* mysqlClose(); */
     return flag;
}

bool accountExists(char *username)
{
     bool flag = false;
     /* mysqlInit(); */
     char sql[200];
     snprintf (sql, 200, "select 1 from %s where %s = '%s' limit 1;",
               tableStr,
               userColumnStr,
               username);
     execSql(sql);
     MYSQL_RES *rs = mysql_store_result(&my_connection);
     if (1 == (unsigned long)mysql_num_rows(rs))
          flag = true;
     mysql_free_result(rs);
     /* mysqlClose(); */
     return flag;
}

bool getPwdFromMysql(char *username, char *password)
{
     bool flag = false;
     /* mysqlInit(); */
     char sql[200];
     snprintf (sql, 200, "select %s from %s where %s = '%s';",
               passwdColumnStr,
               tableStr,
               userColumnStr,
               username);
     execSql(sql);
     MYSQL_RES *rs = mysql_store_result(&my_connection);
     MYSQL_ROW row;
     if (rs) {
          LOG ("query password, return %ld row", (unsigned long)mysql_num_rows(rs));
          if ((row = mysql_fetch_row(rs)) && row[0]) {
               /* *pwdlen = strlen(row[0]); */
               strcpy((char*)password, row[0]);
               flag = true;
          }
          mysql_free_result(rs);
     }
     /* mysqlClose(); */
     return flag;
}

bool updatePwdToMysql(char *username, char *password)
{
     bool flag = false;
     /* mysqlInit(); */
     char sql[200];
     snprintf (sql, 200, "update %s set %s = '%s' where %s = '%s';",
               tableStr,
               passwdColumnStr,
               password,
               userColumnStr,
               username);
     int rows = execSql(sql);
     if (1 == rows)
          flag = true;
     /* mysqlClose(); */
     return flag;
}

bool clearPwdToMysql(char *username)
{
     bool flag = false;
     /* mysqlInit(); */
     char sql[200];
     snprintf (sql, 200, "update %s set %s = null where %s = '%s';",
               tableStr,
               passwdColumnStr,
               userColumnStr,
               username);
     int rows = execSql(sql);
     if (1 == rows)
          flag = true;
     /* mysqlClose(); */
     return flag;
}
