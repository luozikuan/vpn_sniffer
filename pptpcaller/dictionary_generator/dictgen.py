# -*- coding: utf-8 -*-
#! /usr/bin/python
######################################################################
## Filename:      dictgen.py
##                
## Copyright (C) 2013,  Luo Zikuan
## Version:       
## Author:        Luo Zikuan <luozikuan@gmail.com>
##                
## Created at:    Mon Oct 21 18:25:51 2013
## Modified at:   Tue Oct 29 10:31:26 2013
## Modified by:   Luo Zikuan <luozikuan@gmail.com>
##                
## Description:   generate passwd dictionary
######################################################################

import getopt, sys, string

def help_message():
    print """Usage: python dictgen.py [options]
 
 Options:
   -t, --type <a|A|n>      a   --lower alpha
                           A   --upper alpha
                           n   --number
                           default is 'an'
   -l, --length <arg>      number, default is 6
   -o, --output <file>     Place the output into <file>
   -h, --help

 Example:
   python dictgen.py -t Aan -l 6 -o ./dictionary.txt
"""
    sys.exit(0)

typeID = "an"
dictChars = []
length = 6
filename = ""

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:l:o:h?", ["type=", "length=", "output=", "help"])
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(0)

    for o, a in opts:
        if o in ("-t", "--type"):
            typeID = a
        if o in ("-l", "--length"):
            length = int(a)
        if o in ("-o", "--output"):
            filename = a
        if o in ("-h", "--help"):
            help_message()

    # print "typeID =", typeID
    # print "length =", length
    # print "filename =", filename
    
    if 'A' in typeID:
        dictChars = dictChars + list(string.uppercase)
    if 'a' in typeID:
        dictChars = dictChars + list(string.lowercase)
    if 'n' in typeID:
        dictChars = dictChars + list(string.digits)

    if 0 != len(filename):
        f = open(filename, 'w')
        
    result = [dictChars[0]]*length
    base = len(dictChars)
    end = len(dictChars)**length
    i = 0
    while i < end:
        n = i
        sub = length - 1
        while 0 != n:
            result[sub] = dictChars[n % base]
            n = n / base
            sub -= 1
        if 0 == len(filename):
            print "".join(result)
        else:
            f.write("".join(result) + '\n')
        i = i + 1
        
    
    if 0 != len(filename):
        f.close()
