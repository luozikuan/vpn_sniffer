# -*- coding: utf-8 -*-
#! /usr/bin/python

import datetime, string, sys, getopt
from dateutil import parser

filename = ""
length = 6
fromDate = "1980/01/01"
toDate = "1994/12/31"

def help_message():
    print """Usage: python dategen.py [options]
 
 Options:
   -f, --from <date>
   -t, --to <date>
   -l, --length <arg>      6 or 8, 6 is default
   -o, --output <file>     Place the output into <file>
   -h, --help

 Example:
   python dategen.py -f 19920424 -t 19920424 -l 8 -o ./dictionary.txt
"""
    sys.exit(0)

def birthdayPwd(pre, length, start, end, post):
    """generate birthday password
    
    Arguments:
    - `pre`:    pre-attach string
    - `length`: length of birthday, can be 6 or 8
                '680612' is 6,
                '19680612' is 8
    - `start`:  start date, like '19700101'
    - `end`:    end date, like '19700320'
    - `post`:   post-attach string
    """
    if 0 != len(filename):
        f = open(filename, 'w')
    try:
        start = parser.parse(start)
        end = parser.parse(end)
        if end < start:
            start, end = end, start
    except ValueError, e:
        print 'Value error,', e.message
        # sys.exit()
        exit()
    
    if 6 != length and 8 != length:
        length = 6
    cur = start
    while cur <= end:
        if 6 == length:
            content = pre + cur.strftime('%y%m%d') + post
            if 0 != len(filename):
                f.write(content + '\n')
            else:
                print content
        elif 8 == length:
            content = pre + cur.strftime('%Y%m%d') + post
            if 0 != len(filename):
                f.write(content + '\n')
            else:
                print content
        
        cur += datetime.timedelta(days = 1)
    if 0 != len(filename):
        f.close()

if __name__ == '__main__':
    # deal filename
    # ...
    try:
        opts, args = getopt.getopt(sys.argv[1:], "f:t:l:o:h?", ["from=", "to=", "length=", "output=", "help"])
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(0)

    for o, a in opts:
        if o in ("-f", "--from"):
            fromDate = a;
        if o in ("-t", "--to"):
            toDate = a
        if o in ("-l", "--length"):
            length = int(a)
        if o in ("-o", "--output"):
            filename = a
        if o in ("-h", "--help"):
            help_message()
    
    birthdayPwd("", length, fromDate, toDate, "")
