#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <pcap.h>
#include <cstring>
#include <string>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <pptp.h>               // my lib
#include <json/json.h>
#include <unistd.h>
using namespace std;

typedef unsigned char byte;
Json::Value infoList;
string outputFile = "decrypted.pcapng";

void loadInfo(string filename);

void saveFile(const string path, const char *content, int len);
void savePcapHeader();
void savePcapPacket(byte *payload, int payloadlen, bool fromServer);

void decode_packet_c2s(byte *payload, int payloadlen);
void decode_packet_s2c(byte *payload, int payloadlen);
void decode_ip(struct iphdr *headIP, int len, bool fromServer);
void decode_http(const char *payload, int len);

int main(int argc, char *argv[])
{
     bool live = false;
     string pcapFile = "../vpn";
     string deviceStr = "eth0";
     string pwdFolder = "/home/luozikuan/work/dict/";
     int opt;
     while ((opt = getopt(argc, argv, "f:i:p:o:")) != -1) {
          switch (opt) {
          case 'f':
               live = false;
               pcapFile.assign(optarg);
               break;
          case 'i':
               live = true;
               deviceStr.assign(optarg);
               break;
          case 'p':
               pwdFolder.assign(optarg);
               break;
          case 'o':
               outputFile.assign(optarg);
               break;
          default: /* '?' */
               cout<<" Usage: ./prog -f in file [/ -i dev]  "<<endl;
               cout<<"                 -p folder              "<<endl;
               cout<<"               [ -p out file ]          "<<endl;
               exit(EXIT_FAILURE);
          }
     }

     if (pwdFolder == "") {
          cerr<<"must provide -p option"<<endl;
          exit(EXIT_FAILURE);
     }

     loadInfo("../softwareInfo.json");
     cout<<"loaded"<<endl;
     char errbuf[PCAP_ERRBUF_SIZE];

     savePcapHeader();
     
     /* open device */
     pcap_t *device;
     if (live)
          device = pcap_open_live(deviceStr.c_str(), 2048, -1, 500, errbuf);
     else
          device = pcap_open_offline(pcapFile.c_str(), errbuf); 
     if (!device) {
          cerr<<"open pcap_t error"<<endl;
          exit(EXIT_FAILURE);
     }

     // set callback
     strncpy(dict_path, pwdFolder.c_str(), 255);
     Server2Client = decode_packet_s2c;
     Client2Server = decode_packet_c2s;

     /* wait loop forever */
     int cnt = 0;
     pcap_loop(device, -1, getPacket, (u_char *)&cnt);

     /* close device */
     pcap_close(device);

     return 0;
}

void savePcapHeader()
{
     if (0 == access(outputFile.c_str(), F_OK)) { // file exists
          cerr<<outputFile<<" file exists"<<endl;
          if (-1 == remove(outputFile.c_str())) {
               perror("remove file: ");
               exit(EXIT_FAILURE);
          }
     }
     int magic = 0xa1b2c3d4;
     saveFile(outputFile, (char *)&magic, 4);
     short minor = 2, major = 4;
     saveFile(outputFile, (char *)&minor, 2);
     saveFile(outputFile, (char *)&major, 2);
     saveFile(outputFile, "\x00\x00\x00\x00\x00\x00\x00\x00", 8);
     int length = 65535;
     saveFile(outputFile, (char *)&length, 4);
     int type = 113;
     saveFile(outputFile, (char *)&type, 4);
}

void savePcapPacket(byte *payload, int payloadlen, bool fromServer)
{
     struct timeval tv;
     gettimeofday(&tv, NULL);
     saveFile(outputFile, (char *)&tv.tv_sec, 4);
     saveFile(outputFile, (char *)&tv.tv_usec, 4);
     int caplen = payloadlen + 16;
     saveFile(outputFile, (char *)&caplen, 4);
     saveFile(outputFile, (char *)&caplen, 4);

     if (fromServer)
          saveFile(outputFile, "\x00\x00", 2);
     else
          saveFile(outputFile, "\x00\x04", 2);
     saveFile(outputFile, "\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00", 14);

     saveFile(outputFile, (char *)payload, payloadlen);
}

void decode_packet_c2s(byte *payload, int payloadlen)
{
     if (payload[0] & 0x01) {
          if (PPP_IP == payload[0]) {
               payload += 1;
               payloadlen -= 1;
          }
     } else {
          if (0x00 == payload[0] && PPP_IP == payload[1]) {
               payload += 2;
               payloadlen -= 2;
          }
     }
     decode_ip((struct iphdr *)payload, payloadlen, false);
}

void decode_packet_s2c(byte *payload, int payloadlen)
{
     if (payload[0] & 0x01) {
          if (PPP_IP == payload[0]) {
               payload += 1;
               payloadlen -= 1;
          }
     } else {
          if (0x00 == payload[0] && PPP_IP == payload[1]) {
               payload += 2;
               payloadlen -= 2;
          }
     }
     decode_ip((struct iphdr *)payload, payloadlen, true);
}

void decode_ip(struct iphdr *headerIP, int len, bool fromServer)
{
     savePcapPacket((byte *)headerIP, len, fromServer);

     if (6 != headerIP->protocol)
          return ;

     struct tcphdr *headerTcp = (struct tcphdr *)(headerIP + 1);
     int lenTcp = len - sizeof(struct iphdr);

     u_char *payloadTcp = (u_char *)headerTcp + headerTcp->doff * 4;
     int payloadlenTcp = lenTcp - headerTcp->doff * 4;

     if (!fromServer) {
          decode_http((char *)payloadTcp, payloadlenTcp);
     }
     
}

// void getPacket(unsigned char *arg,
//                const struct pcap_pkthdr *pkthdr,
//                const unsigned char *bytes)
// {
//      int *cnt = (int *)arg;
//      (*cnt)++;
//      struct ether_header *headerEther = (struct ether_header *)bytes;
//      int lenEther = pkthdr->caplen;
//      if (ETHERTYPE_IP != ntohs(headerEther->ether_type))
//           return ;
     
//      struct iphdr *headerIP = (struct iphdr *)((char *)headerEther + sizeof(struct ether_header));
//      int lenIP = lenEther - sizeof(struct ether_header);
//      if (6 != headerIP->protocol)
//           return ;
     
//      struct tcphdr *headerTcp = (struct tcphdr *)((char *)headerIP + sizeof(struct iphdr));
//      int lenTcp = lenIP - sizeof(struct iphdr);

//      u_char *payload = (u_char *)headerTcp + headerTcp->doff * 4;
//      int lenPayload = lenTcp - headerTcp->doff * 4;

//      decode_http((char*)payload, lenPayload);
// }

void loadInfo(string filename)
{
     ifstream in(filename.c_str());
     if (!in) {
          cerr<<"open "<<filename<<" error."<<endl;
          exit(EXIT_FAILURE);
     }

     Json::Reader reader;
     if (!reader.parse(in, infoList)) {
          cerr<<"parse error"<<endl;
          exit(EXIT_FAILURE);
     }
     in.close();
}

int hexchar2int(char ch)
{
     if (isdigit(ch))
          return ch - '0';
     else
          return toupper(ch) - 'A' + 10;
}

string convert(string str)
{
     string result;
     char ch;
     char t1, t2;
     unsigned int i;
     for (i = 0; i < str.length(); i++) {
          ch = str[i];
          if ('%' == ch) {
               if (i + 2 < str.length()){
                    t1 = str[++i];
                    t2 = str[++i];
                    if (isxdigit(t1) && isxdigit(t2)) {
                         ch = hexchar2int(t1) * 16 + hexchar2int(t2);
                         result += ch;
                    } else {
                         result += '%';
                         result += t1;
                         result += t2;
                    }
               } else {
                    result += '%';
               }
          } else if ('+' == ch) {
               result += ' ';
          } else {
               result += ch;
          }
     }
     return result;
}

void saveFile(const string path, const char *content, int len)
{
     ofstream out(path.c_str(), ios::binary|ios::app);
     if (!out) {
          cerr<<"save file "<<path<<" error!"<<endl;
          exit(EXIT_FAILURE);
     }
     out.write(content, len);
     //out<<"\n==============================\n";
     out.close();
}

void decode_http(const char *payload, int len)
{
     string content;
     content.assign(payload, len);
     
     string hostname, nextLine = "\r\n";
     string::size_type posHostname = content.find("Host: ");
     if (string::npos == posHostname) // can't find host
          return ;

     posHostname += 6;
     string::size_type posNextLine = content.find(nextLine, posHostname);
     hostname = content.substr(posHostname, posNextLine - posHostname);
     
     unsigned int i;
     for (i = 0; i < infoList["array"].size(); i++) {
          if (infoList["array"][i]["host"].asString() == hostname)
               break;
     }
     if (i == infoList["array"].size())
          return ;

     // check whether loginStr exists
     if (string::npos == content.find(infoList["array"][i]["loginStr"].asString())) {
          return ;
     }
     
     string userName, passwd;
     string::size_type posUserName = content.find(infoList["array"][i]["userBegin"].asString());
     posUserName += infoList["array"][i]["userBegin"].asString().length();
     if (infoList["array"][i]["userEnd"].asString() != ""){
          string::size_type posAmp = content.find(infoList["array"][i]["userEnd"].asString(), posUserName);
          userName = content.substr(posUserName, posAmp - posUserName);
     } else {
          userName = content.substr(posUserName);
     }

     string::size_type posPasswd = content.find(infoList["array"][i]["passwdBegin"].asString());
     posPasswd += infoList["array"][i]["passwdBegin"].asString().length();
     if (infoList["array"][i]["passwdEnd"].asString() != ""){
          string::size_type posAmp = content.find(infoList["array"][i]["passwdEnd"].asString(), posPasswd);
          passwd = content.substr(posPasswd, posAmp - posPasswd);
     } else {
          passwd = content.substr(posPasswd);
     }

     userName = convert(userName);
     passwd = convert(passwd);
     cout<<"========================================"<<endl;
     cout<<"software: "<<infoList["array"][i]["name"].asString()<<endl;
     cout<<"username: "<<userName<<endl;
     cout<<"password: "<<passwd<<endl;
}
